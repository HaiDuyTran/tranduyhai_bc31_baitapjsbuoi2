/**
Input:
    Lương 1 ngày:100.000
    Số ngày làm:30

Các bước xử lý:
-Tạo 2 biến lưu giá trị lương 1 ngày và số ngày làm
-Tạo biến lưu giá trị tiền lương
-Sử dụng toán tử *

Out put
    tienLuong = ?
 */
function tienLuong() {
  var luong1NgayValue = document.getElementById("txt-luong1Ngay").value *1;
  console.log("luong1NgayValue",luong1NgayValue)
  var soNgayLamValue = document.getElementById("txt-soNgayLam").value *1;
  console.log("soNgayLamValue",soNgayLamValue)
  var tienLuong = luong1NgayValue * soNgayLamValue;
  console.log("tienLuong", tienLuong);
  document.getElementById("tienLuong").innerHTML=
  `<div>Tiền lương mỗi người :`+ tienLuong +`</div>`
}
