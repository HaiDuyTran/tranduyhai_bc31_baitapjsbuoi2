/**
 Input:
    Giá trị 5 số thực

Các bước xử lý:
-Tạo ra 5 biến số thực
-Tạo biến lưu giá trị trung bình
-Công thức:Tổng 5 số thực chia 5

Output:
    soTb = ?

 */
function soTB() {
  var sothu1Value = document.getElementById("txt-sothu1").value * 1;
  console.log("sothu1Value", sothu1Value);
  var sothu2Value = document.getElementById("txt-sothu2").value * 1;
  console.log("sothu2Value", sothu2Value);
  var sothu3Value = document.getElementById("txt-sothu3").value * 1;
  console.log("sothu3Value", sothu3Value);
  var sothu4Value = document.getElementById("txt-sothu4").value * 1;
  console.log("sothu4Value", sothu4Value);
  var sothu5Value = document.getElementById("txt-sothu5").value * 1;
  console.log("sothu5Value", sothu5Value);
  var soTB =
    (sothu1Value + sothu2Value + sothu3Value + sothu4Value + sothu5Value) / 5;
  console.log("soTB", soTB);
  document.getElementById("soTB").innerHTML =
    `<div>Số trung bình là :  ` + soTB + `</div>`;
}
