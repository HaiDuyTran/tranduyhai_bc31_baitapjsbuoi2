/** 
Input:
    cDai:5cm
    cRong:4cm

Các bước xử lý:
-Tạo 2 biến lưu giá trị cDai và cRong hcn
-Tạo 2 biến lưu giá trị chu vi và diện tích hcn
-Công thức tính chu vi:(cDai+cRong)*2
-Công thức tính diện tích:cDai*cRong

Output:
    chuVi = ?
    dienTich = ?
*/
function tinhToan() {
  var cDaiValue = document.getElementById("txt-cDai").value * 1;
  console.log("cDaiValue", cDaiValue);
  var cRongValue = document.getElementById("txt-cRong").value * 1;
  console.log("cRongValue", cRongValue);
  var chuVi = (cDaiValue + cRongValue) * 2;
  console.log("chuVi", chuVi);
  var dienTich = cDaiValue * cRongValue;
  console.log("dienTich", dienTich);
  document.getElementById("chuVi").innerHTML=`<div>Chu vi hcn :`+chuVi+`</div>`
  document.getElementById("dienTich").innerHTML=`<div>Dien tich hcn :`+dienTich+`</div>`
}
