/** 
Input:
    giaUSD:23.500
    soTien:2

Các bước xử lý:
-Tạo 2 biến lưu giá trị giaUSD và soTienQuyDoi
-Tạo biến lưu giá trị tiền sau khi quy đổi
-Sử dụng toán tử *

Output:
    tienQuyDoi = ?
*/
function tinhTien() {
  var giaUSD = 23500;
  var soTienValue = document.getElementById("txt-soTien").value * 1;
  console.log("soTienValue", soTienValue);
  var tinhTien = soTienValue * giaUSD;
  console.log("tinhTien", tinhTien);
  document.getElementById("tinhTien").innerHTML =
    `<div>Số tiền quy đổi là :` + tinhTien + `</div>`;
}
